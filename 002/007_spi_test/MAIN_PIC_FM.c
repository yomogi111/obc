#include <MAIN_PIC_FM.h>
#fuses HS, NOWDT, NOPROTECT, PUT, BROWNOUT, NOLVP
#use spi (MASTER, SPI1, ENABLE=PIN_C2, MODE=0, BITS=8, STREAM=SPI_1)
 
#define SPIPORT SPI_1
#define CS_PIN PIN_D2


#define str_length 270
#define data_length 255
//Apply debug output, If you need debug output, elase "//"(Double slash) on #7
#define DEBUG


#define ENABLE_WRITE 0x06
#define READ_ID      0x9F
#define WRITE_DATA   0x12
#define ERASE_DATA   0x21
#define ERASE_SUBSECTOR 0xDC
#define READ_DATA    0x13


void WRITE_ENABLE_OF()
{  
   output_low(CS_PIN);              //build connection
   spi_xfer(SPIPORT,ENABLE_WRITE);  //send command"0x06"
   output_high(CS_PIN);             //close connection
   return;
}

void sector_erase_of()//unsigned int32 sector_address)
{
   int32 sector_address = 0xFFFFFFFF;
   unsigned int8 adress[4];
   adress[0] = (unsigned int8)((sector_address>>24) & 0xFF);
   adress[1] = (unsigned int8)((sector_address>>16) & 0xFF);
   adress[2] = (unsigned int8)((sector_address>>8) & 0xFF);
   adress[3] = (unsigned int8)((sector_address) & 0xFF);
   
   WRITE_ENABLE_OF();
   output_low(CS_PIN);
   delay_us(2);
   spi_xfer(SPIPORT,ERASE_DATA);
   spi_xfer(SPIPORT,adress[0]);
   spi_xfer(SPIPORT,adress[1]);
   spi_xfer(SPIPORT,adress[2]);
   spi_xfer(SPIPORT,adress[3]);
   
   delay_us(2);
   output_high(CS_PIN);
   
   puts("erase now,,,,,,,,,,,,,,,,,,,,,,,,,,,,");
   delay_ms(750);
   
   return;
}

void SUBSECTOR_4KB_ERASE_SCF()//unsigned int32 sector_address)                      //Funcion que borra un sector de 4KB de la COM Flash
{
   int32 sector_address = 0xFFFFFFFF;
   unsigned int8 adsress[4];
   
   adsress[0]  = (unsigned int8)((sector_address>>24) & 0xFF);                   // 0x _ _ 00 00 00
   adsress[1]  = (unsigned int8)((sector_address>>16) & 0xFF);                   // 0x 00 _ _ 00 00
   adsress[2]  = (unsigned int8)((sector_address>>8) & 0xFF);                    // 0x 00 00 _ _ 00
   adsress[3]  = (unsigned int8)((sector_address) & 0xFF);                       // 0x 00 00 00 _ _
   
   
   WRITE_ENABLE_OF();                                                              //Funcion que habilita escritura en COM Flash
   output_low(CS_PIN);                                                         //lower the CS PIN
   delay_us(2);
   
  spi_xfer(SPIPORT,ERASE_SUBSECTOR); //SECTOR ERASE COMAND   (0xDC)
   
   spi_xfer(SPIPORT,adsress[0]);   
   spi_xfer(SPIPORT,adsress[1]);    
   spi_xfer(SPIPORT,adsress[2]);    
   spi_xfer(SPIPORT,adsress[3]);
   
   delay_us(2);
   output_high(CS_PIN);                                                        //take CS PIN higher back
   puts("erase now,,,,,,,,,,,,,,,,,,,,,,,,,,,,");
   delay_ms(10);  
   
   return;
}

void WRITE_DATA_BYTE()                 //write data to flash memory
{  
   int8 data = 0x32;          //0x32==2
   //int8 data2 = 0x33;         //0x33==3
   //int8 data3 = 0x34;         //0x34==4
   unsigned int8 adress[4];
   adress[0] = (unsigned int8) 0xFF;
   adress[1] = (unsigned int8) 0xFF;
   adress[2] = (unsigned int8) 0xFF;
   adress[3] = (unsigned int8) 0x01;
   WRITE_ENABLE_OF();
   output_low(CS_PIN);
   
   spi_xfer(SPIPORT,WRITE_DATA);       //send command "0x12"
   spi_xfer(SPIPORT,adress[0]);        //send write adress
   spi_xfer(SPIPORT,adress[1]);
   spi_xfer(SPIPORT,adress[2]);
   spi_xfer(SPIPORT,adress[3]);
   spi_xfer(SPIPORT,data);             //write data
   //spi_xfer(SPIPORT,data2);             //write data
  // spi_xfer(SPIPORT,data3);             //write data
   output_high(CS_PIN);
   puts(" : write complete!");
   return;
}

char READ_DATA_BYTE()
{  
   int8 i;
   unsigned int8 adress[4];
   adress[0] = (unsigned int8) 0xFF;
   adress[1] = (unsigned int8) 0xFF;
   adress[2] = (unsigned int8) 0xFF;
   adress[3] = (unsigned int8) 0x01;
   int8 data;
   
   output_low(CS_PIN);
   
   for(i=0;i<8;i++)
   {
      adress[0] = (unsigned int8) 0xFF;
      adress[1] = (unsigned int8) 0xFF;
      adress[2] = (unsigned int8) 0xFF;
      adress[3] = (unsigned int8) 0x01+i;
      spi_xfer(SPIPORT,READ_DATA);     //send command "0x13"
      spi_xfer(SPIPORT,adress[0]);
      spi_xfer(SPIPORT,adress[1]);
      spi_xfer(SPIPORT,adress[2]);
      spi_xfer(SPIPORT,adress[3]);
      data = spi_xfer(SPIPORT);        //read data
      printf(" [ %d ] 0%d from flash memory\n\r",data,i);
   }
  
   
   output_high(CS_PIN);
   puts(" : read complete!");
   return data;
}

void main()
{  
   char cmd;
   char prntwrd;
   printf("---------------------------------------------------------------\n\r");
   
   while (TRUE)
   {
      cmd = getc();
      switch(cmd)
      {
         case '0' :
         case '1' :
         case '2':
         case '3' :
         case '4' :
         case '5' :
         case '6' :
         case '7' :
         case '8' :
         case '9' : output_high(PIN_A1);
                  puts("   LED PinMode : HIGH");   break;
        /* case "w" : prntwrd = WRITE_DATA_BYTE();
                  printf("  %s  from flash memory",prntwrd);
                                                   break;
         case r : READ_DATA();                     break;*/
         default: if((cmd == 'r') || (cmd == 'w')) break;
                  else
                  output_low(PIN_A1);
                  puts("   LED PinMode : LOW");    break;
      }
      if(cmd == 'w')
      {  
         WRITE_DATA_BYTE();
      }else if(cmd == 'r')
      {
         prntwrd = READ_DATA_BYTE();
         //printf(" [ %d ] from flash memory\n\r",prntwrd);
      }else if(cmd == 'e')
      {
         sector_erase_of();
         SUBSECTOR_4KB_ERASE_SCF();
         puts(" : erase complete");
      }
      
      
      if(cmd == 'q')
      {
         output_low(CS_PIN);
         spi_xfer(SPIPORT,90);
         output_high(CS_PIN);
         printf(" : ID >>> %d \r\n",prntwrd);
      }
 
      delay_ms(1000);
   }
}
