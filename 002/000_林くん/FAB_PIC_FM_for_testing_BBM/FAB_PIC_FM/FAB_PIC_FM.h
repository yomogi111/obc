#include <16F1789.h>
#device ADC=12
#use delay(internal=16MHz)
#use rs232(baud=9600,parity=N,xmit=PIN_C6,rcv=PIN_C7,bits=8,stream=MPIC,ERRORS)
#include <PIC16F1789_registers.h>
#include <define_func.c>
/*#fuses WDT, NOMCLR*/
