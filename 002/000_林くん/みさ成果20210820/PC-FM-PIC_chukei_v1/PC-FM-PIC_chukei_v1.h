#include <16F1789.h>
#device ADC=12

#FUSES PUT                   	//Power Up Timer
#FUSES BROWNOUT              	//Reset when brownout detected
#FUSES NOWRT                 	//Program memory not write protected
#FUSES BORV25                	//Brownout reset at 2.5V
#FUSES LPBOR                 	//Low-Power Brownout reset is enabled

#use delay(oscillator=16MHz)
#use rs232(baud=115200,parity=N,xmit=PIN_C6,rcv=PIN_C7,bits=8,stream=PORT1)
#use rs232(baud=115200,parity=N,xmit=PIN_D2,rcv=PIN_D3,bits=8,stream=PORT2,FORCE_SW)

