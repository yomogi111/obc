#include <16F1789.h>
#device ADC=12
#use delay(oscillator=16MHz)
#use rs232(baud=14400,parity=N,xmit=PIN_C6,rcv=PIN_C7,bits=8,stream=PORT1)
#use spi (MASTER, CLK=PIN_C3, DI=PIN_C4, DO=PIN_C5, BAUD=14400, BITS=8, STREAM=SPI_1, MODE=0)

