# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO
from time import sleep
import datetime
import spidev

import numpy as np
import matplotlib.pyplot as plt

from decimal import Decimal, ROUND_HALF_UP, ROUND_HALF_EVEN

class sensor:

    Vref = 3.3
    ADRANGE = 4096

    def __init__(self,
                CSB=0,                 
                baud=1000000,  
                bus=0):


        self.h = spidev.SpiDev()

        self.h.open(bus, CSB)

        #1MHz
        self.h.max_speed_hz = baud
        self.h.mode = 0



    def readadc(self, adcnum):
        if adcnum > 7 or adcnum < 0:
            return -1
    
        ch0 = [0x06,0x00,0x00]
        upperbit = adcnum & 0x04
        ch0[0] = (upperbit >> 2) | ch0[0]
        lowerbits = adcnum & 0x03
        ch0[1] = (lowerbits << 6) | ch0[1]


        
        adc = self.h.xfer2(ch0)
        #print(adc)

        data = ((adc[1] & 0x0f) << 8) | adc[2]
        #print(str(self.Vref*data/4096) + "V")
        #print('>>', Decimal(str(self.Vref*data/4096)).quantize(Decimal('0.01'), rounding=ROUND_HALF_UP),'V')


        return data

    def readLM60BIZ(self, adcnum):

        addata = self.readadc(adcnum)

        return (addata/4096*self.Vref - 0.424) /(6.25/1000)
    
    def readLM61BIZ(self, adcnum):

        addata = self.readadc(adcnum)

        return (addata/4096*self.Vref - 0.600) /(10/1000)
    #return is cm
    def readGP2Y0A21YK0F(self, adcnum):
    
        addata = self.readadc(adcnum)

        #mm
        #https://blog.obniz.com/ja/blog/%E8%B5%A4%E5%A4%96%E7%B7%9A%E8%B7%9D%E9%9B%A2%E3%82%BB%E3%83%B3%E3%82%B5%E3%83%BC-gp2y0a21yk0f-2/
        #distance = 19988.34 * pow((addata / self.Vref) * 4096, -1.25214) * 10


        #http://myct.jp/arduino/index.php?%E8%B7%9D%E9%9B%A2%E3%82%BB%E3%83%B3%E3%82%B5+GP2Y0A21YK0F
        distance = 26.549 * pow( (addata / 4096 * self.Vref), -1.2091) 
        

        return distance
    

    



if __name__ == '__main__':

    sensor = sensor(1)

    x = []
    y = []
    plt.ion()

    try:
        while True:

            
            dt_now =datetime.datetime.now()
            timeString = dt_now.strftime('%Y/%m/%d %H:%M:%S')
        
            inputVal0 = sensor.readadc(0)
            temp =  sensor.readLM61BIZ(0) 
            print(timeString + "," + str(inputVal0) + "," + str(temp)  + '\n')
            DataFile = open('tempData.csv','a')
            DataFile.write(timeString + "," + str(inputVal0) + "," + str(temp)  + '\n')

            x.append(dt_now)
            y.append(temp)
            plt.plot(x, y, color ='red')
            plt.draw()
            plt.show()

            DataFile.close()
        #plt.pause(60*15)
            plt.pause(15)
        #sleep(1)

    except KeyboardInterrupt:
        pass

    GPIO.cleanup()
    DataFile.close()
