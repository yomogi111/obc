# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO
from time import sleep
import datetime
import spidev
import time


from decimal import Decimal, ROUND_HALF_UP, ROUND_HALF_EVEN

class flash:

    Vref = 3.3
    ADRANGE = 4096

    def __init__(self,
                CSB=0,                 
                baud=1000000,  
                bus=0):


        self.h = spidev.SpiDev()

        self.h.open(bus, CSB)

        #1MHz
        self.h.max_speed_hz = baud
        self.h.mode = 0


   
    def SUBSECTOR_4KB_ERACE_SMF(self,sector_address):
        print("-------- SMF 4KB sector erace START --------")
        cmd = [0x06,0x21]
        sector_address = [0x00,0x00,0x00,0x00]
        self.h.xfer2(cmd)
        self.h.xfer2(sector_address)
        time.sleep(0.01)
        print("-------- SMF 4KB sector erace complete --------")
        return
    
    def sector_erace_SMF(self,sector_address):
        print("-------- SMF sector erace START --------")
        print("-------- SMF sector erace complete --------")
        return
    
    def WRITE_DATA_BYTE_SMF(self,page_address,data):
        print("-------- write data to SMF START --------")
        print("-------- write data to SMF complete --------")
        return
    
    def READ_DATA_BYTE_SMF(self,ADDRESS):
        print("-------- read SMF data START --------")
        cmd = [0x12]
        adress[4]
        sector_address = [0x00,0x00,0x00,0x00]
        self.h.xfer2(cmd)
        self.h.xfer2(sector_address)
        time.sleep(0.01)
        print("-------- read SMF data complete --------")
        return
    
    def WRITE_ENABLE_SMF():
        return

    def read_chip_id(self):
        cmd = [0x9F,0x00]
        chip_id = self.h.xfer2(cmd)
        print(chip_id[0])
        print(chip_id[1])
        print("CHIP ID >>>", chip_id[0] )
        return chip_id


if __name__ == '__main__':

    Flash = flash(1)
    

  
    try:
        while True:

            
        
            inputVal0 = Flash.read_chip_id()
        #plt.pause(60*15)
            print(inputVal0)        #sleep(1)

            sleep(5)
    except KeyboardInterrupt:
        pass

    GPIO.cleanup()

