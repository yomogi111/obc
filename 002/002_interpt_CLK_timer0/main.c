#include <main.h>
#fuses HS,NOWDT,NOPROTECT,PUT,BROWNOUT,NOLVP
#use rs232(UART1,baud = 9600)

//#use fast_io(A1)
#int_timer0
void intval()
{
   static int counter = 0;
   static int flag = 0;
   counter++;
   if(counter > 61)
   {
      counter = 0;
      if(flag == 0)
      {
        // output_high(PIN_A1);
         flag = 1;
         puts("flag >>> 1");
      }else{
       //  output_low(PIN_A1);
         flag = 0;
         puts("flag >>> 0");
      }
   }
}

void main()
{  
   char cmd;
   set_tris_b(0);
   
   setup_timer_0(RTCC_INTERNAL | RTCC_DIV_256);
   set_timer0(0);
   
   enable_interrupts(INT_TIMER0);
   enable_interrupts(GLOBAL);
   
   while(1)
   {
      cmd = getc();
      if((cmd >= '0') && (cmd <= '9'))
      {
         output_high(PIN_D3);
         puts("   LED PinMode : HIGH");
      }else{
         output_low(PIN_D3);
         puts("   LED PinMode : LOW");
      }
      delay_ms(1000);
   }
}



/*
void main()
{  
   char cmd;
   while(1)
   {
      cmd = getc();
      //putc(cmd);
      if((cmd >= '0') && (cmd <= '9'))
      {
         //printf("number\r\n");
         output_high(PIN_A1);
         //printf("HI");
         puts("   LED PinMode : HIGH");
         //putc('*');
      }else{
         //putc(0xD);
         output_low(PIN_A1);
         //printf("num");
         puts("   LED PinMode : LOW");
         //putc('*');
      }
      //delay_cycles(1);
      delay_ms(1000);
      
      //delay_cycles(1);
      //delay_ms(500);
   }
}
*/

